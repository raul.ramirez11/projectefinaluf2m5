import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Assert;
import org.junit.Test;

public class JunitTests {

	@Test
	public void testTreballarDates() throws ParseException {
		float[] array= {2.0f,01012020.0f,02022020.0f};
		int res = principal.TreballarDates(array);
		try 
		{
			Assert.assertEquals(32,res);
		}catch(AssertionError e) 
		{
			System.out.println("TreballarDates Error: " + e);
			throw e;
		}
		
		System.out.println("TreballarDates OK");
	}
	
	@Test
	public void testMostrarResultado() throws ParseException {
		float[] array= {1.0f,10.0f,30.0f};
		String res = principal.MostrarResultado(array);
		try 
		{
			Assert.assertEquals("NO",res);
		}catch(AssertionError e) 
		{
			System.out.println("MostrarResultado Error: " + e);
			throw e;
		}
		
		System.out.println("MostrarResultado OK");
	}

}
