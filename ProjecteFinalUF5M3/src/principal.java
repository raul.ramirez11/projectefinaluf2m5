import java.util.Scanner;
import java.util.Date;
import java.util.InputMismatchException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
/**
 * 
 * <h2>Classe principal, es on estan tots el metodes i funcions</h2>
 * @author Raul Ramirez
 * @version 1.0
 *
 */
public class principal {
	/**
	 * main
	 * En el metode main es treballara amb les diferents clases y funcions
	 * @param args Creat automaticamente
	 * @throws ParseException Senyal que s'ha arribat a un error inesperadament al analitzar
	 */
		public static void main(String[] args) throws ParseException {
			int control=0;
			int num2=0;
			int num=0;
			do {
				try {
					Scanner n2 = new Scanner(System.in);
					num=n2.nextInt();
					
					while(num2<num) {
						LlenarVector(n2);
						num2++;
					}
					n2.close();
					control=1;
				}catch(InputMismatchException e) {
					System.out.println("Error: " + e);
				}
			}while(control==0);

		}
	/**
	 * LlenarVector
	 * En aquest metode s'omple l'array amb el que treballarem
	 * @param n Objecte scanner per reutilitzar Scanner de la classe anterior
	 * @return result
	 * @throws ParseException Senyal que s'ha arribat a un error inesperadament al analitzar
	 */
		public static int LlenarVector(Scanner n) throws ParseException 
		{
			//Scanner n =new Scanner(System.in);
			int result = 0;
			float array[] = new float[3];
			for (int i = 0; i < array.length; i++) {
				try 
				{
					if(array[0] > 2 || array[0] < 0)
					{
						System.out.println("El valor " + array[0] + " no cumple los requisitos");
						i=0;
						array[i]=n.nextFloat();
					}
					else
						array[i]=n.nextFloat();
				}
				catch(InputMismatchException e) 
				{
					
					System.out.println("Error: " + e);
					n = new Scanner(System.in);
					i--;
				}
			}
			if(array[2]<array[1]) {
				do {
					System.out.println(array[1]+">"+array[2]);
					array[1]=n.nextFloat();
				}while(array[2]>=array[1]);
			}
			if(array[0]==2) {
				TreballarDates(array);
			}
			else MostrarResultado(array);
			return result;
		}
	/**
	 * TreballarDates
	 * En cas d'haver escollit l'opcio dates, aquest metode serveix per veure la diferencia en dies d'una data a una altre
	 * @param array Es pasa array de bools de la classe LlenarVector
	 * @return dies Numero de dies
	 * @throws ParseException Senyal que s'ha arribat a un error inesperadament al analitzar
	 */
		public static int TreballarDates(float array[]) throws ParseException {
			int dies=0;
			SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyyyy");
			Date [] date = new Date[array.length];
			String array2[] = new String[array.length];
			for(int i = 1; i < array.length; i++) {
				array2[i] = "" + Math.round(array[i]);
				date[i] = formatDate.parse(array2[i]);
				if(array2[i].length() !=8) {
					array2[i] = "0"+array2[i];
					date[i] = formatDate.parse(array2[i]);
				}
				else {
					date[i] = formatDate.parse(array2[i]);
				}
			}
			int diferencia=(int) ((date[2].getTime()-date[1].getTime())/1000);
			
	        if(diferencia>86400) {
	            dies=(int)Math.floor(diferencia/86400);
	            diferencia=diferencia-(dies*86400);
	        }
	        System.out.println(dies);
			return dies;
		}
		/**
		 * MostrarResultado
		 * Depenen de l'opcio tindrem un resultat o un altre. Diferenciador si tens presencialitat o semipresencialitat
		 * @param array el array de la funcio LlenarArray
		 * @throws ParseException Senyal que s'ha arribat a un error inesperadament al analitzar
		 */
		public static String MostrarResultado(float array[]) throws ParseException {
			float result = array[1]/array[2];
			int opcion=Math.round(array[0]);
			int percent= Math.round(result*100);
			String mensaje = "";
			switch(opcion) 
			{
				case 0://NOSemipresencial
					if(percent<80)
						mensaje = "NO";
					else
						mensaje = "SI";
					break;
				case 1://Semipresencial
					if(percent<50)
						mensaje = "NO";
					else
						mensaje = "SI";
					break;
			}
			System.out.println(mensaje);
			return mensaje;
		}
}
